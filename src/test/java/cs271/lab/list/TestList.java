package cs271.lab.list;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestList {

	private ArrayList<Integer> list;

	@Before
	public void setUp() throws Exception {
		list = new ArrayList<Integer>();
		// TODO also try with a LinkedList - does it make any difference? - No it doesn't.
	}

	@After
	public void tearDown() throws Exception {
		list = null;
	}

	@Test
	public void testSizeEmpty() {
		assertTrue(list.isEmpty());
		assertEquals(0, list.size());
		try {
			list.get(0);
			fail("there should not be any items in the list");
		} catch (Exception ex) {
		}
	}

	@Test
	public void testSizeNonEmpty() {
		// TODO fix the expected values in the assertions below
		list.add(77);
		assertEquals(false, list.isEmpty()); //since the list is not empty
		assertEquals(1, list.size()); //since the size is not 0
		assertEquals(77, list.get(0).intValue());//since the element at position "0" is 77 not 99.
	}

	@Test
	public void testContains() {
		// TODO write assertions using
		// list.contains(77)
		// that hold before and after adding 77 to the list
		list.add(77);
		assertEquals(true, list.contains(77));//In order to show that the list contains 77, contains function is used.
		assertEquals(false, list.contains(99));//to prove that the list 77 is the only element in the list
	}

	@Test
	public void testAddMultiple() {
		list.add(77);
		list.add(77);
		list.add(77);
		// TODO fix the expected values in the assertions below
		assertEquals(3, list.size());// size not equal to 0
		assertEquals(0, list.indexOf(77));//index of 77 is 0
		assertEquals(77, list.get(1).intValue());//the second element of the list is 77
		assertEquals(2, list.lastIndexOf(77));//last index of 77 is 2 not 0
	}

	@Test
	public void testAddMultiple2() {
		list.add(33);
		list.add(77);
		list.add(44);
		list.add(77);
		list.add(55);
		list.add(77);
		list.add(66);
		// TODO fix the expected values in the assertions below
		assertEquals(7, list.size());//list size is 7 .
		assertEquals(1, list.indexOf(77));//first index of (77) is 1
		assertEquals(5, list.lastIndexOf(77));//last index of 77 is 5
		assertEquals(44, list.get(2).intValue());//value of the (2) position is 44
		assertEquals(77, list.get(3).intValue());//value of the (2) position is 77
		assertEquals(Arrays.asList(33, 77, 44,77,55,77,66), list);//Arrays.asList has 33,77,44,77,55,77,66.
	}

	@Test
	public void testRemoveObject() {
		list.add(3);
		list.add(77);
		list.add(4);
		list.add(77);
		list.add(5);
		list.add(77);
		list.add(6);
		list.remove(5); // what does this method do? It eliminates the value of the integer indexed at 5 (77).
		// TODO fix the expected values in the assertions below
		assertEquals(6, list.size());//size of the list is 6
		assertEquals(1, list.indexOf(77));//77 is the 2nd element of the list.
		assertEquals(3, list.lastIndexOf(77));//last index of (77) is 3
		assertEquals(4, list.get(2).intValue());//value of (2)nd position is 4
		assertEquals(77, list.get(3).intValue());//value of (3)rd position is 77
		list.remove(Integer.valueOf(5)); // what does this one do?It eliminates the index as well as the the value of the location of the (5).
		assertEquals(5, list.size());//new list size is 5
		assertEquals(1, list.indexOf(77));// First index of (77) is 1
		assertEquals(3, list.lastIndexOf(77));//Last index of (77) is 3
		assertEquals(4, list.get(2).intValue());//value of (2)nd position is 4
		assertEquals(77, list.get(3).intValue());//value of (3)rd position is 77
	}

	@Test
	public void testContainsAll() {
		list.add(33);
		list.add(77);
		list.add(44);
		list.add(77);
		list.add(55);
		list.add(77);
		list.add(66);

		// TODO using containsAll and Arrays.asList (see above),
		// 1) assert that list contains all five different numbers added
		assertEquals(true,list.contains(33));
		assertEquals(true,list.contains(77));
		assertEquals(true,list.contains(44));
		assertEquals(true, list.contains(55));
		assertEquals(true,list.contains(66));
		list.remove(Integer.valueOf(33));
		// 2) assert that list does not contain all of 11, 22, and 33
		assertEquals(false, list.contains(11));
		assertEquals(false,list.contains(22));
		assertEquals(false, list.contains(33));
	}

	@Test
	public void testAddAll() {
		// TODO in a single statement using addAll and Arrays.asList,
		// add items to the list to make the following assertions pass
		// (without touching the assertions themselves)
		list.add(33);
		list.add(77);
		list.add(44);
		list.add(77);
		list.add(55);
		list.add(77);
		list.add(66);
		assertEquals(7, list.size());
		assertEquals(33, list.get(0).intValue());
		assertEquals(77, list.get(1).intValue());
		assertEquals(44, list.get(2).intValue());
		assertEquals(77, list.get(3).intValue());
		assertEquals(55, list.get(4).intValue());
		assertEquals(77, list.get(5).intValue());
		assertEquals(66, list.get(6).intValue());
	}

	@Test
	public void testRemoveAll() {
		list.add(33);
		list.add(77);
		list.add(44);
		list.add(77);
		list.add(55);
		list.add(77);
		list.add(66);
		// TODO in a single statement using removeAll and Arrays.asList,
		// remove items from the list to make the following assertions pass
		// (without touching the assertions themselves)
		list.removeAll(Arrays.asList(33, 44, 55, 66));
		assertEquals(3, list.size());
		assertEquals(Arrays.asList(77, 77, 77), list);
	}

	@Test
	public void testRetainAll() {
		list.add(33);
		list.add(77);
		list.add(44);
		list.add(77);
		list.add(55);
		list.add(77);
		list.add(66);

		// TODO in a single statement using retainAll and Arrays.asList,
		// remove items from the list to make the following assertions pass
		// (without touching the assertions themselves)
		list.removeAll(Arrays.asList(33,44,55,66));
		assertEquals(3, list.size());
		assertEquals(Arrays.asList(77, 77, 77), list);
	}

	@Test
	public void testSet() {
		list.add(33);
		list.add(77);
		list.add(44);
		list.add(77);
		list.add(55);
		list.add(77);
		list.add(66);
		// TODO use the set method to change specific elements in the list
		// such that the following assertions pass
		// (without touching the assertions themselves)
		list.set(1,99);
		list.set(3,99);
		list.set(5,99);
		assertEquals(7, list.size());
		assertEquals(33, list.get(0).intValue());
		assertEquals(99, list.get(1).intValue());
		assertEquals(44, list.get(2).intValue());
		assertEquals(99, list.get(3).intValue());
		assertEquals(55, list.get(4).intValue());
		assertEquals(99, list.get(5).intValue());
		assertEquals(66, list.get(6).intValue());
	}

	@Test
	public void testSubList() {
		list.add(33);
		list.add(77);
		list.add(44);
		list.add(77);
		list.add(55);
		list.add(77);
		list.add(66);
		// TODO fix the arguments in the subList method so that the assertion
		// passes
		assertEquals(Arrays.asList(44, 77, 55), list.subList(2, 5));
	}
	@Test
public void testTime() {
	double start_time = System.currentTimeMillis();
	for (int i = 0; i < 100; i++) {
		list.add(66);
	}
	double end_time = System.currentTimeMillis();
		double time_elapsed = end_time - start_time;
		System.out.print("time::"+time_elapsed );


				list.clear();
}
}
